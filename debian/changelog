golang-gopkg-errgo.v1 (1.0.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Apply multi-arch hints. + golang-gopkg-errgo.v1-dev: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 25 Nov 2022 01:45:02 +0000

golang-gopkg-errgo.v1 (1.0.1-1) unstable; urgency=medium

  * New upstream version 1.0.1
  * Add debian/watch to track v1 release tarballs
  * debian/gbp.conf: Set debian-branch to debian/sid for DEP-14 conformance
  * Apply "cme fix dpkg" fixes:
    - Update debhelper dependency to "Build-Depends: debhelper-compat (= 12)"
    - Bump Standards-Version to 4.5.0 (no change)
  * Remove unused "Files-Excluded: vendor Godeps/_workspace"
    in debian/copyright
  * Update dependency as per upstream go.mod: switch from
    golang-gopkg-check.v1-dev to golang-github-frankban-quicktest-dev
  * Add "Rules-Requires-Root: no" to debian/control
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Adopt package from Alexandre Viau (Closes: #940443)

 -- Anthony Fok <foka@debian.org>  Sun, 16 Feb 2020 03:19:42 -0700

golang-gopkg-errgo.v1 (0.0~git20161222.442357a-3) unstable; urgency=medium

  * Team upload.
  * Vcs-* urls: pkg-go-team -> go-team.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Feb 2018 00:18:14 -0500

golang-gopkg-errgo.v1 (0.0~git20161222.442357a-2) unstable; urgency=medium

  * point Vcs-* urls to salsa.d.o subproject

 -- Alexandre Viau <aviau@debian.org>  Thu, 25 Jan 2018 17:05:48 -0500

golang-gopkg-errgo.v1 (0.0~git20161222.442357a-1) unstable; urgency=medium

  * Initial release (Closes: #886598)

 -- Alexandre Viau <aviau@debian.org>  Sun, 07 Jan 2018 19:51:23 -0500
